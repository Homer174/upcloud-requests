<?php

class ServerRequests
{
    const SERVER_UUID = '0027b726-648d-4448-a6c6-530e8b0f3755';
    const AUTH_HEADER = 'Basic YXBpMjpyQ2ZOYjlGOEdodw==';
    const API_URL = 'https://api.upcloud.com/1.3/server/';

    /*
     * cURL
     */
    private $curl;

    public function __construct()
    {
        $this->curl = curl_init();
    }

    public function getServerDetails() {
        curl_setopt_array($this->curl, [
            CURLOPT_URL => self::API_URL . self::SERVER_UUID,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => '',
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 30,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => 'GET',
            CURLOPT_POSTFIELDS => '',
            CURLOPT_HTTPHEADER => [
                'Authorization: ' . self::AUTH_HEADER,
            ],
        ]);

        $response = curl_exec($this->curl);
        $err = curl_error($this->curl);

        curl_close($this->curl);

        if ($err) {
            return 'cURL Error #:' . $err;
        } else {
            return json_decode($response);
        }
    }

    public function serverStart() {
        curl_setopt_array($this->curl, [
            CURLOPT_URL => self::API_URL . self::SERVER_UUID . '/start',
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => '',
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 30,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => 'POST',
            CURLOPT_POSTFIELDS => '',
            CURLOPT_HTTPHEADER => [
                'Authorization: ' . self::AUTH_HEADER
            ],
        ]);

        $response = curl_exec($this->curl);
        $err = curl_error($this->curl);

        curl_close($this->curl);

        if ($err) {
            return 'cURL Error #:' . $err;
        } else {
            return json_decode($response);
        }
    }

    public function serverStop() {
        curl_setopt_array($this->curl, [
            CURLOPT_URL => self::API_URL . self::SERVER_UUID . '/stop',
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => '',
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 30,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => 'POST',
            CURLOPT_POSTFIELDS => '',
            CURLOPT_HTTPHEADER => [
                'Authorization: ' . self::AUTH_HEADER,
            ],
        ]);

        $response = curl_exec($this->curl);
        $err = curl_error($this->curl);

        curl_close($this->curl);

        if ($err) {
            return 'cURL Error #:' . $err;
        } else {
            return json_decode($response);
        }
    }
}
